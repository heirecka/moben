# Copyright 2016 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=digint tag=v${PV} ]
require systemd-service

SUMMARY="Tool for creating snapshots and remote backups of btrfs subvolumes"
DESCRIPTION="
btrbk is a backup tool for btrfs subvolumes, taking advantage of btrfs specific capabilities to create atomic snapshots and transfer them incrementally to your backup locations.
btrbk is designed to run as a cron job for triggering periodic snapshots and backups, as well as from the command line
"
HOMEPAGE+=" https://digint.ch/${PN}/"
DOWNLOADS="https://digint.ch/download/${PN}/releases/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc[>=8.6.0]
    run:
        dev-lang/perl:*
        sys-fs/btrfs-progs[>=3.18.2]
    recommendation:
        net-misc/openssh [[ description = [ remote backups via ssh ] ]]
    suggestion:
        app-misc/pv [[ description = [ used for rate limiting and progress bar with remote backups ] ]]
        (
            app-arch/lz4
            app-arch/xz
            app-arch/pigz
            app-arch/gzip
            app-arch/bzip2
            app-arch/pbzip2
            app-arch/lzo
        ) [[ *description = [ compression support for transferring from/to remote locations ] ]]

"

BUGS_TO="moben@exherbo.org"

DEFAULT_SRC_INSTALL_PARAMS=(
    BINDIR=/usr/$(exhost --target)/bin
    DOCDIR=/usr/share/doc/${PNVR}
    SYSTEMDDIR="${SYSTEMDSYSTEMUNITDIR}"
)

src_install() {
    default

    edo gzip -d -f "${IMAGE}"/usr/share/doc/${PNVR}/*.gz    \
                   "${IMAGE}"/usr/share/man/*/*.gz
}

