# Copyright 2012 Benedikt Morbach <benedikt.morbach@googlemail.com>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]
require cmake [ api=2 ] gtk-icon-cache python [ blacklist=3 multibuild=false ]

SUMMARY="A portable open-source implementation of Bioware's Infinity Engine"
DESCRIPTION="
GemRB is a portable open-source implementation of Bioware's Infinity Engine. It was written to
support pseudo-3D role playing games based on the Dungeons & Dragons ruleset, such as the Baldur's
Gate and Icewind Dale series and Planescape: Torment.
"
HOMEPAGE="http://gemrb.org"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        media-libs/freetype
        media-libs/glew
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/openal [[ note = [ preferred, could also use SDL_mixer:2 for fast lower-quality sound ] ]]
        media-libs/SDL:2[X]
        x11-dri/mesa [[ note = [ provides libGL ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DDATA_DIR:PATH=/usr/share/${PN}
        -DDOC_DIR:PATH=/usr/share/doc/${PNVR}
        -DICON_DIR:PATH=/usr/share/pixmaps
        -DMAN_DIR:PATH=/usr/share/man/man6
        -DMENU_DIR:PATH=/usr/share/applications
        -DSVG_DIR:PATH=/usr/share/icons/hicolor/scalable/apps
        -DSYSCONF_DIR:PATH=/etc/${PN}
        -DDISABLE_WERROR:BOOL=TRUE
        -DSDL_BACKEND:STRING=SDL2
        -DOPENGL_BACKEND:STRING=OpenGL
        -DUSE_FREETYPE:BOOL=TRUE
        -DUSE_LIBVLC:BOOL=FALSE
        -DUSE_OPENAL:BOOL=TRUE
        -DUSE_PNG:BOOL=TRUE
        -DUSE_SDLMIXER:BOOL=FALSE
        -DUSE_VORBIS:BOOL=TRUE
)

src_prepare() {
    cmake_src_prepare

    # fix python shebang
    edo sed \
        -e 's:/usr/bin/python:/usr/bin/python2:g' \
        -i admin/extend2da.py
}

